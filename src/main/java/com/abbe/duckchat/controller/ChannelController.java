package com.abbe.duckchat.controller;

import com.abbe.duckchat.model.Channel;
import com.abbe.duckchat.service.ChannelService;
import com.abbe.duckchat.websocket.ChannelSocketHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ChannelController {

    @Autowired
    ChannelSocketHandler channelSocketHandler;
    @Autowired
    ChannelService channelService;

    @GetMapping("/channels")
    public ResponseEntity<List<Channel>> getAllChannels() {
        return ResponseEntity.ok(channelService.getAll());
    }

    @PostMapping("/channels")
    public ResponseEntity<List<Channel>> addChannel(@RequestBody Channel channel) {
        channelService.save(channel);
        List<Channel> channelList = channelService.getAll();
        channelSocketHandler.broadcast(channel.getTitle() + " Chat channel was created, join the channel ID: " + channel.getId());
        return ResponseEntity.status(201).body(channelList);
    }

    @DeleteMapping("channels/{id}")
    public ResponseEntity<List<Channel>> deleteChannel(@PathVariable("id") long id) {
        channelService.delete(id);
        List<Channel> channelList = channelService.getAll();
        channelSocketHandler.broadcast("Channel with ID: " + id + " is now removed.");
        return ResponseEntity.status(201).body(channelList);
    }


}
