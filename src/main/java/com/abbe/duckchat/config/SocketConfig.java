package com.abbe.duckchat.config;

import com.abbe.duckchat.websocket.ChannelSocketHandler;
import com.abbe.duckchat.websocket.UserChatSocketHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;


@Configuration
@EnableWebSocket
public class SocketConfig implements WebSocketConfigurer {

    @Autowired
    private ChannelSocketHandler channelSocketHandler;

    @Autowired
    private UserChatSocketHandler userChatSocketHandler;

    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        registry.addHandler(userChatSocketHandler, "/sub/chat/*");
        registry.addHandler(channelSocketHandler, "/sub/channels");


    }

}
