package com.abbe.duckchat.repository;

import com.abbe.duckchat.model.Channel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ChannelRepository extends JpaRepository<Channel, Long> {
}
