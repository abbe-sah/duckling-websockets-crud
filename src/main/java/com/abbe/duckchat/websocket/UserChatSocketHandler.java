package com.abbe.duckchat.websocket;

import com.abbe.duckchat.model.ChatUserHistory;
import com.abbe.duckchat.repository.ChannelRepository;
import com.abbe.duckchat.service.ChannelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Component
public class UserChatSocketHandler extends TextWebSocketHandler {
    private HashMap<WebSocketSession, String> sessionIdAndPath = new HashMap();
    private List<WebSocketSession> userSessions = new ArrayList<>();
    private List<ChatUserHistory> userSessionsHistory = new ArrayList<>();

    @Autowired
    ChannelRepository channelRepository;

    @Autowired
    ChannelService channelService;

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        broadcast(sessionIdAndPath.get(session), message.getPayload());

    }

    public void broadcast(String id, String message) {
        for (WebSocketSession webSessions : userSessions) {
            try {
                String path = webSessions.getUri().getPath();
                String cid = path.substring(path.lastIndexOf('/') + 1);
                if (id.equals(cid)) {
                    webSessions.sendMessage(new TextMessage("MESSAGE: " + message));
                    addToHistory(cid, "Old Messages: " + message);
                }


            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        userSessions.add(session);
        String path = session.getUri().getPath();
        String spath = path.substring(path.lastIndexOf('/') + 1);
        sessionIdAndPath.put(session, spath);
        for (ChatUserHistory history : userSessionsHistory) {
            if (history.getChannelID().equals(spath)) {
                session.sendMessage(new TextMessage(history.getMessages()));
            }
        }
        if (channelService.get(Long.valueOf(spath)) == null) {
            session.sendMessage(new TextMessage("This channel do not exist "));
        } else {
            session.sendMessage(new TextMessage("Welcome to the user channel " + channelService.get(Long.valueOf(spath)).getTitle()));
        }

    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        sessionIdAndPath.remove(session);
        userSessions.remove(session);
    }

    public void addToHistory(String channelId, String oldMessage) {
        ChatUserHistory chatUserHistory = new ChatUserHistory();
        chatUserHistory.setChannelID(channelId);
        chatUserHistory.setMessages(oldMessage);
        userSessionsHistory.add(chatUserHistory);
    }

}