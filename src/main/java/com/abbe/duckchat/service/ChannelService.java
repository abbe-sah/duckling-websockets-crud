package com.abbe.duckchat.service;

import com.abbe.duckchat.model.Channel;
import com.abbe.duckchat.repository.ChannelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ChannelService {

    @Autowired
    ChannelRepository channelRepository;

    public List<Channel> getAll() {
        return channelRepository.findAll();
    }

    public Channel save(Channel channel) {
        return channelRepository.save(channel);
    }

    public void delete(long id) {
        Channel currentChannel = channelRepository.getReferenceById(id);
        channelRepository.delete(currentChannel);
    }

    public Channel get(long id) {
        Optional<Channel> Channel = channelRepository.findById(id);
        if (Channel.isPresent()) {
            return Channel.get();
        } else {
            throw new IllegalArgumentException("There is no id");
        }
    }

}
