package com.abbe.duckchat;

import com.abbe.duckchat.model.Channel;
import com.abbe.duckchat.repository.ChannelRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class DuckChatApplication {

    public static void main(String[] args) {
        SpringApplication.run(DuckChatApplication.class, args);
    }

    @Bean
    public CommandLineRunner run(ChannelRepository channelRepository) {
        return args -> {
            Channel channel = new Channel();
            channel.setTitle("MAIN CHAT");
            channelRepository.save(channel);

        };

    }
}
